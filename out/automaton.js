define(["require", "exports", "Enums", "language/Messages", "mod/Mod", "Utilities"], function (require, exports, Enums_1, Messages_1, Mod_1, Utilities) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class Automaton extends Mod_1.Mod {
        constructor() {
            super(...arguments);
            this.isActivated = false;
            this.movesLeft = 0;
            this.loopsNumber = Automaton.DEFAULT_LOOPS_NUMBER;
            this.lastScript = "";
            this.currentAction = 0;
            this.actions = [
                this.buildAction(Enums_1.ActionType.Gather),
                this.buildAction(Enums_1.ActionType.Move, (player) => {
                    return { direction: player.facingDirection };
                }),
            ];
        }
        onInitialize(saveDataGlobal) {
            this.keyBind = this.addKeyBind(this.getName(), Automaton.BIND_KEY);
        }
        onLoad(saveData) {
            this.modMessage = this.addMessage("modMessage", "Mod: _0_");
            if (saveData !== undefined) {
                this.lastScript = saveData;
            }
        }
        onSave() {
            return this.lastScript;
        }
        onKeyDown(event) {
            if (this.isActivated) {
                this.abortAutomation("Aborted by player");
            }
            return undefined;
        }
        onMouseDown(event) {
            if (this.isActivated) {
                this.abortAutomation("Aborted by player");
            }
            return undefined;
        }
        onShowInGameScreen() {
            this.elementDialog = this.buildDialog();
        }
        onNoInputReceived(player) {
            this.makeAutomationStep();
        }
        onKeyBindPress(keyBind) {
            switch (keyBind) {
                case this.keyBind:
                    ui.toggleDialog(this.elementDialog);
                    return false;
            }
            return undefined;
        }
        buildAction(actionType, actionArgument) {
            return (player) => {
                let action = (actionArgument === undefined) ? undefined : actionArgument(player);
                actionManager.execute(localPlayer, actionType, action);
            };
        }
        buildDialog() {
            const inputId = "action-type-input-text";
            const submitButtonClass = "action-type-submit-button";
            const self = this;
            let elementContainer = $('<div>'
                + `<input class="dialog-input" id="${inputId}" value="${self.lastScript}"/>`
                + `<button class="${submitButtonClass}">></button>` +
                '</div>');
            elementContainer.on("click", `.${submitButtonClass}`, function () {
                let inputValue = $(`#${inputId}`).val();
                self.lastScript = inputValue;
                self.setupActionsByString(inputValue);
                self.activateAutomation();
            });
            return this.createDialog(elementContainer, {
                id: this.getName(),
                title: "Automaton",
                x: 20,
                y: 180,
                width: 200,
                height: "auto",
                resizable: true,
            });
        }
        setupActionsByString(script) {
            let [description, count] = script.split(":", 2);
            this.actions[0] = this.generateActionByString(description);
            let loopsNumber = parseInt(count);
            this.loopsNumber = loopsNumber ? loopsNumber : Automaton.DEFAULT_LOOPS_NUMBER;
        }
        generateActionByString(s) {
            let actionType;
            let actionArgument;
            switch (s.trim().toLowerCase()) {
                case 'harvest':
                case 'h':
                    actionType = Enums_1.ActionType.Harvest;
                    break;
                case 'gather grass':
                    return this.buildGatherGrassAction();
                case 'carve':
                case 'c':
                    actionType = Enums_1.ActionType.Carve;
                    break;
                default:
                    actionType = Enums_1.ActionType.Gather;
                    actionArgument = (player) => {
                        return {
                            item: this.findItemsFor(player.inventory, actionType)[0]
                        };
                    };
            }
            return this.buildAction(actionType, actionArgument);
        }
        activateAutomation() {
            this.movesLeft = this.loopsNumber * this.actions.length;
            this.isActivated = true;
            this.currentAction = 0;
            ui.displayMessage(localPlayer, this.modMessage, Messages_1.MessageType.Stat, "Started!");
            game.passTurn(localPlayer);
        }
        makeAutomationStep() {
            if (!this.isActivated) {
                return;
            }
            if (this.movesLeft <= 0) {
                ui.displayMessage(localPlayer, this.modMessage, Messages_1.MessageType.Stat, "Done");
                this.isActivated = false;
                return;
            }
            this.movesLeft -= 1;
            this.actions[this.currentAction++ % this.actions.length](localPlayer);
        }
        findItemsFor(container, actionType) {
            let items = [];
            for (let item of container.containedItems) {
                let itemActions = item.description().use;
                if (itemActions && itemActions.some(action => action === actionType)) {
                    items.push(item);
                }
            }
            return items;
        }
        buildGatherGrassAction() {
            return (player) => {
                ui.displayMessage(localPlayer, this.modMessage, Messages_1.MessageType.Stat, Enums_1.ActionType[Enums_1.ActionType.Till]);
                let x = player.x + player.direction.x;
                let y = player.y + player.direction.y;
                let z = player.z;
                let tile = game.getTile(x, y, z);
                let terrainTypeBeforeAction = getTerrainType(tile);
                if (Automaton.BLOCKING_TERRAIN_TYPES.indexOf(terrainTypeBeforeAction) !== -1) {
                    this.abortAutomation(`${Enums_1.TerrainType[terrainTypeBeforeAction]} is on the way.`);
                    return;
                }
                if (tile.doodad) {
                    this.abortAutomation(`You can't work because a ${tile.doodad.description().name} is on the way.`);
                    return;
                }
                if (tile.containedItems) {
                    this.abortAutomation(`You can't work because of some items on the ground.`);
                    return;
                }
                let tillItem = this.findItemsFor(player.inventory, Enums_1.ActionType.Till)[0];
                if (!tillItem) {
                    this.abortAutomation("Can't find a suitable tool in your inventory.");
                    return;
                }
                if (tillItem.minDur === 1) {
                    this.abortAutomation(`${tillItem.description().name} almost broken.`);
                    return;
                }
                if (player.stats.stamina.value <= 1) {
                    this.abortAutomation("You're exhausted.");
                    return;
                }
                let creature = this.isCreatureNearby(player);
                if (creature) {
                    this.abortAutomation(`You can't work because of ${creature.description().name}`);
                    return;
                }
                if (terrainTypeBeforeAction !== Enums_1.TerrainType.Grass) {
                    return;
                }
                actionManager.execute(localPlayer, Enums_1.ActionType.Till, {
                    item: tillItem
                });
                let terrainTypeAfterAction = getTerrainType(tile);
                if (terrainTypeAfterAction === Enums_1.TerrainType.Grass) {
                    this.currentAction--;
                    this.movesLeft++;
                }
            };
        }
        isCreatureNearby(player) {
            let radius = Automaton.DEFAULT_CREATURE_RADAR_RADIUS;
            for (let creature of game.creatures) {
                if (creature !== undefined && creature.isInFov() && !creature.pet() && player.z === creature.z) {
                    if (Math.abs(player.x - creature.x) < radius && Math.abs(player.y - creature.y) < radius) {
                        return creature;
                    }
                }
            }
            return undefined;
        }
        abortAutomation(message) {
            this.movesLeft = 0;
            ui.displayMessage(localPlayer, this.modMessage, Messages_1.MessageType.Stat, message);
        }
    }
    Automaton.DEFAULT_LOOPS_NUMBER = 5;
    Automaton.DEFAULT_CREATURE_RADAR_RADIUS = 3;
    Automaton.BLOCKING_TERRAIN_TYPES = [
        Enums_1.TerrainType.Lava,
        Enums_1.TerrainType.CaveEntrance,
        Enums_1.TerrainType.Rocks,
        Enums_1.TerrainType.RocksWithCoal,
        Enums_1.TerrainType.RocksWithIron,
        Enums_1.TerrainType.RocksWithLimestone,
        Enums_1.TerrainType.RocksWithTalc,
        Enums_1.TerrainType.Sandstone,
        Enums_1.TerrainType.SandstoneWithIron,
        Enums_1.TerrainType.SandstoneWithNiter,
    ];
    Automaton.BIND_KEY = 72;
    exports.default = Automaton;
    function getTerrainType(tile) {
        return Utilities.TileHelpers.getType(tile);
    }
});
//# sourceMappingURL=automaton.js.map