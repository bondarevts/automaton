import {ActionType, KeyBind, TerrainType} from "Enums";
import {MessageType} from "language/Messages";
import {IPlayer} from "player/IPlayer";
import {Mod} from "mod/Mod";
import {ExecuteArgument} from "action/IAction";
import {IContainer, IItem} from "item/IItem";
import {ITile} from "tile/ITerrain";
import * as Utilities from "Utilities";
import {ICreature} from "creature/ICreature";


// noinspection JSUnusedGlobalSymbols
export default class Automaton extends Mod {

    private static readonly DEFAULT_LOOPS_NUMBER: number = 5;
    private static readonly DEFAULT_CREATURE_RADAR_RADIUS = 3;
    private static readonly BLOCKING_TERRAIN_TYPES: TerrainType[] = [
        TerrainType.Lava,
        TerrainType.CaveEntrance,
        TerrainType.Rocks,
        TerrainType.RocksWithCoal,
        TerrainType.RocksWithIron,
        TerrainType.RocksWithLimestone,
        TerrainType.RocksWithTalc,
        TerrainType.Sandstone,
        TerrainType.SandstoneWithIron,
        TerrainType.SandstoneWithNiter,
    ];
    private static readonly BIND_KEY: number = 72;  // H

    private keyBind: number;
    private modMessage: number;
    private isActivated: boolean = false;
    private movesLeft: number = 0;
    private loopsNumber: number = Automaton.DEFAULT_LOOPS_NUMBER;

    private elementDialog: JQuery;
    private lastScript: string = "";

    private currentAction: number = 0;
    private actions: { (player: IPlayer): void } [] = [
        this.buildAction(ActionType.Gather),
        this.buildAction(ActionType.Move, (player: IPlayer) => {
            return {direction: player.facingDirection}
        }),
    ];

    onInitialize(saveDataGlobal: any): void {
        this.keyBind = this.addKeyBind(this.getName(), Automaton.BIND_KEY);
    }

    public onLoad(saveData: any): void {
        this.modMessage = this.addMessage("modMessage", "Mod: _0_");
        if (saveData !== undefined) {
            this.lastScript = saveData;
        }
    }

    public onSave(): any {
        return this.lastScript;
    }

    public onKeyDown(event: JQueryEventObject): boolean | any {
        if (this.isActivated) {
            this.abortAutomation("Aborted by player");
        }
        return undefined;
    }

    public onMouseDown(event: JQueryEventObject): boolean | any {
        if (this.isActivated) {
            this.abortAutomation("Aborted by player");
        }
        return undefined;
    }

    public onShowInGameScreen(): void {
        this.elementDialog = this.buildDialog();
    }

    public onNoInputReceived(player: IPlayer): void {
        this.makeAutomationStep();
    }

    public onKeyBindPress(keyBind: KeyBind): boolean {
        switch (keyBind) {
            case this.keyBind:
                ui.toggleDialog(this.elementDialog);
                return false;
        }
        return undefined;
    }

    private buildAction(actionType: ActionType, actionArgument?: (player: IPlayer) => ExecuteArgument): (player: IPlayer) => void {
        return (player: IPlayer) => {
            let action: ExecuteArgument = (actionArgument === undefined) ? undefined : actionArgument(player);
            actionManager.execute(localPlayer, actionType, action);
        };
    }

    private buildDialog(): JQuery {
        const inputId: string = "action-type-input-text";
        const submitButtonClass: string = "action-type-submit-button";

        // The dialog-input class handles key press events
        const self = this;
        let elementContainer: JQuery = $(
            '<div>'
                + `<input class="dialog-input" id="${inputId}" value="${self.lastScript}"/>`
                + `<button class="${submitButtonClass}">></button>` +
            '</div>'
        );

        elementContainer.on("click", `.${submitButtonClass}`, function () {
            let inputValue = $(`#${inputId}`).val();
            self.lastScript = inputValue;
            self.setupActionsByString(inputValue);
            self.activateAutomation();
        });

        return this.createDialog(elementContainer, {
            id: this.getName(),
            title: "Automaton",
            x: 20,
            y: 180,
            width: 200,
            height: "auto",
            resizable: true,
        });
    }

    private setupActionsByString(script: string): void {
        let [description, count] = script.split(":", 2);
        this.actions[0] = this.generateActionByString(description);
        let loopsNumber: number = parseInt(count);
        this.loopsNumber = loopsNumber ? loopsNumber : Automaton.DEFAULT_LOOPS_NUMBER;
    }

    private generateActionByString(s: string): { (player: IPlayer): void } {
        let actionType: ActionType;
        let actionArgument: (player: IPlayer) => ExecuteArgument;
        switch (s.trim().toLowerCase()) {
            case 'harvest':
            case 'h':
                actionType = ActionType.Harvest;
                break;
            case 'gather grass':
                return this.buildGatherGrassAction();
            case 'carve':
            case 'c':
                actionType = ActionType.Carve;
                break;
            default:
                actionType = ActionType.Gather;
                actionArgument = (player: IPlayer) => {
                    return {
                        item: this.findItemsFor(player.inventory, actionType)[0]
                    }
                };
        }
        return this.buildAction(actionType, actionArgument);
    }

    private activateAutomation(): void {
        this.movesLeft = this.loopsNumber * this.actions.length;
        this.isActivated = true;
        this.currentAction = 0;
        ui.displayMessage(localPlayer, this.modMessage, MessageType.Stat, "Started!");
        game.passTurn(localPlayer);
    }

    private makeAutomationStep(): void {
        if (!this.isActivated) {
            return;
        }
        if (this.movesLeft <= 0) {
            ui.displayMessage(localPlayer, this.modMessage, MessageType.Stat, "Done");
            this.isActivated = false;
            return;
        }
        this.movesLeft -= 1;
        this.actions[this.currentAction++ % this.actions.length](localPlayer);
    }

    private findItemsFor(container: IContainer, actionType: ActionType): IItem[] {
        let items: IItem[] = [];
        for (let item of container.containedItems) {
            let itemActions = item.description().use;
            if (itemActions && itemActions.some(action => action === actionType)) {
                items.push(item);
            }
        }
        return items;
    }

    private buildGatherGrassAction(): (player: IPlayer) => void {
        return (player: IPlayer) => {
            ui.displayMessage(localPlayer, this.modMessage, MessageType.Stat, ActionType[ActionType.Till]);

            let x = player.x + player.direction.x;
            let y = player.y + player.direction.y;
            let z = player.z;

            let tile: ITile = game.getTile(x, y, z);
            let terrainTypeBeforeAction = getTerrainType(tile);
            if (Automaton.BLOCKING_TERRAIN_TYPES.indexOf(terrainTypeBeforeAction) !== -1) {
                this.abortAutomation(`${TerrainType[terrainTypeBeforeAction]} is on the way.`);
                return;
            }
            if (tile.doodad) {
                this.abortAutomation(`You can't work because a ${tile.doodad.description().name} is on the way.`);
                return;
            }
            if (tile.containedItems) {
                this.abortAutomation(`You can't work because of some items on the ground.`);
                return;
            }
            let tillItem: IItem = this.findItemsFor(player.inventory, ActionType.Till)[0];
            if (!tillItem) {
                this.abortAutomation("Can't find a suitable tool in your inventory.");
                return;
            }
            if (tillItem.minDur === 1) {
                this.abortAutomation(`${tillItem.description().name} almost broken.`);
                return;
            }
            if (player.stats.stamina.value <= 1) {
                this.abortAutomation("You're exhausted.");
                return;
            }
            let creature = this.isCreatureNearby(player);
            if (creature) {
                this.abortAutomation(`You can't work because of ${creature.description().name}`);
                return;
            }

            if (terrainTypeBeforeAction !== TerrainType.Grass) {
                return;  // skip tile
            }
            actionManager.execute(localPlayer, ActionType.Till, {
                item: tillItem
            });
            let terrainTypeAfterAction = getTerrainType(tile);
            if (terrainTypeAfterAction === TerrainType.Grass) {
                this.currentAction--;
                this.movesLeft++;
            }
        };
    }

    private isCreatureNearby(player: IPlayer): ICreature|undefined {
        let radius: number = Automaton.DEFAULT_CREATURE_RADAR_RADIUS;
        for (let creature of game.creatures) {
            if (creature !== undefined && creature.isInFov() && !creature.pet() && player.z === creature.z) {
                if (Math.abs(player.x - creature.x) < radius && Math.abs(player.y - creature.y) < radius) {
                    return creature;
                }
            }
        }
        return undefined;
    }

    private abortAutomation(message: string) {
        this.movesLeft = 0;
        ui.displayMessage(localPlayer, this.modMessage, MessageType.Stat, message);
    }
}


function getTerrainType(tile: ITile): TerrainType {
    return Utilities.TileHelpers.getType(tile);
}
